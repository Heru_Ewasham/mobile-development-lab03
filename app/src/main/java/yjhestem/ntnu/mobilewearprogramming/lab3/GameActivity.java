package yjhestem.ntnu.mobilewearprogramming.lab3;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;

import static android.graphics.BitmapFactory.*;

// Base code from https://androidkennel.org/android-sensors-game-tutorial/

public class GameActivity extends AppCompatActivity implements SensorEventListener2 {

    private static final String TAG = "GameActivity";
    private float xPos, xAccel, xVel = 0.0f;
    private float yPos, yAccel, yVel = 0.0f;
    private float xMax, yMax;
    private float velOpposite = -1f;
    private int ballWidth = 50;
    private int ballHeight = 50;
    private int rectX, rectY;
    private Boolean hitted = false;
    private Bitmap ball;
    private SensorManager sensorManager;
    private Vibrator vibrator;
    private MediaPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        //ballHeight = (int) 0.05*size.x;
        //ballWidth = (int) 0.05*size.x;
        rectX = 50;
        rectY = 50;
        xMax = (float) (size.x - 0.05*size.x);
        yMax = (float) (size.y - 0.05*size.x - 200);
        xPos = (float) size.x/2;
        yPos = (float) size.y/2;
        Log.d(TAG, "onCreate: xMax: " + xMax);
        Log.d(TAG, "onCreate: yMax: " + yMax);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        player = MediaPlayer.create(this,R.raw.collision);

        BallView ballView = new BallView(this);
        setContentView(ballView);

    }

    @Override
    protected void onStart() {
        super.onStart();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        //sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onStop() {
        sensorManager.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onFlushCompleted(Sensor sensor) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            xAccel = -sensorEvent.values[0];            //We need to do this for Landscape
            yAccel = -sensorEvent.values[1];
            updateBall();
        }
        /*if (sensorEvent.sensor.getType() == Sensor.TYPE_GRAVITY) {
            xAccel = sensorEvent.values[0];
            yAccel = -sensorEvent.values[1];
            updateBall();
        }*/
    }

    private void updateBall() {
        float frameTime = 0.666f;
        xVel += (xAccel * frameTime);
        yVel += (yAccel * frameTime);

        float xS = (xVel / 2) * frameTime;
        float yS = (yVel / 2) * frameTime;

        // We need this way for landscape.
        yPos -= xS;
        xPos -= yS;

        if (xPos > xMax) {
            xPos = xMax;
            //xVel = xVel*velOpposite;
            onHit();
        } else if (xPos < rectX) {
            xPos = rectX;
            //xVel = xVel*velOpposite;
            onHit();
        }

        if (yPos > yMax) {
            yPos = yMax;
            //yVel = yVel*velOpposite;
            onHit();
        } else if (yPos < rectY) {
            yPos = rectY;
            //yVel = yVel*velOpposite;
            onHit();
        }

        // If not hitting and hitted is true, set to false.
        if (hitted && yPos < yMax && xPos <xMax && yPos > rectY && xPos > rectX) {
            hitted = false;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void onHit() {
        // If not already "played", play sound and vibrate.
        if(!hitted) {
            player.start();
            // Vibrate for 100 milliseconds
            int vTime = 100;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(vTime, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                vibrator.vibrate(vTime);
            }
            hitted = true;
            //xPos = rectX/2;
            //yPos = rectY/2;
        }
    }


    private class BallView extends View {

        private Rect rectangle;
        private Paint paint;

        public BallView(Context context) {
            super(context);

            // Draw rectangle-part found at: https://alvinalexander.com/android/how-to-draw-rectangle-in-android-view-ondraw-canvas


            // create a rectangle that we'll draw later
            rectangle = new Rect(rectX, rectY, Math.round(xMax)+ballWidth, Math.round(yMax)+ballHeight); //Find the correct place.

            // create the Paint and set its color
            paint = new Paint();
            paint.setColor(Color.BLACK);
            // Drawing rectangle-part in ballview finished.

            Bitmap ballSrc = decodeResource(getResources(), R.drawable.ball);

            ball = Bitmap.createScaledBitmap(ballSrc, ballWidth, ballHeight, true);


        }

        @Override
        protected void onDraw(Canvas canvas) {
            //canvas.drawColor(Color.BLUE);
            canvas.drawRect(rectangle, paint);
            canvas.drawBitmap(ball, xPos, yPos, null);
            Log.d("Canvas", "onDraw: Canvas width" + canvas.getWidth());
            Log.d("Canvas", "onDraw: Canvas height" + canvas.getHeight());
            invalidate();
        }
    }
}
